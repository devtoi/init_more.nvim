command! -nargs=* EditPerProjectConfig lua require("init_more").edit_config()
command! -nargs=* LoadPerProjectConfig lua require("init_more").load_project_config()
