local pathSeparator = (function()
	local jit = require("jit")
	if jit then
		local os = string.lower(jit.os)
		if os == "linux" or os == "osx" or os == "bsd" then
			return "/"
		else
			return "\\"
		end
	else
		return "/"
	end
end)()

local plugin = {
	silent = true,
	location_types = {
		CurrentWorkingDirectory = 0,
		ProjectsFolder = 1,
	},
	accepted_checksums = {},
	after_config_loaded = nil,
	after_all_config_loaded = nil,
}

plugin.options = {
	projects_directory = vim.fn.stdpath("config") .. pathSeparator .. "projects" .. pathSeparator,
	load_from_projects_directory = true,
	-- Poses security risks
	load_from_current_directory = false,
	load_lua_config = true,
	load_vim_config = false,
	potential_sources = {
		{ location_type = plugin.location_types.ProjectsFolder, file_name = "nvim_config.lua" },
		{ location_type = plugin.location_types.CurrentWorkingDirectory, file_name = "nvim_config.lua" },
		{ location_type = plugin.location_types.ProjectsFolder, file_name = "nvim_config.vim" },
		{ location_type = plugin.location_types.CurrentWorkingDirectory, file_name = "nvim_config.vim" },
	},
	accepted_checksums_file_name = "accepted_checksums.txt",
}

local function get_project_name_from_directory(directory)
	return string.match(directory, "[^%" .. pathSeparator .. "]+$")
end

local function get_directory_from_location_type(options, locationType)
	if locationType == plugin.location_types.ProjectsFolder then
		-- Assume the project name is the name of the current directory
		local project_name = get_project_name_from_directory(vim.loop.cwd())
		return vim.fn.expand(options.projects_directory .. project_name)
	elseif locationType == plugin.location_types.CurrentWorkingDirectory then
		return vim.loop.cwd()
	end
end

function plugin.get_checksum(file_to_be_checksummed_path)
	local file = io.popen("sha512sum " .. file_to_be_checksummed_path)
	local line = file:read("*l")
	local checksum = nil
	for str in string.gmatch(line, "([^ ]+)") do
		checksum = str
		break
	end
	file:close()
	return checksum
end

local function ask_user_to_accept_config(options, file_path)
	local answer = vim.fn.input("Do you want to load the configuration file " .. file_path .. "? [y/N]: ")

	if answer ~= "" then
		local option = string.lower(string.sub(answer, 1, 1))
		if option == "y" then
			return true
		end
	end

	return false
end

local function add_checksum_to_accepted_cache(checksum, file_path)
	if not plugin.accepted_checksums then
		plugin.accepted_checksums = {}
	end
	plugin.accepted_checksums[file_path] = checksum
end

local function get_accepted_checksums_file_directory(options)
	return get_directory_from_location_type(options, plugin.location_types.ProjectsFolder)
end

local function get_accepted_checksums_file(options)
	return get_accepted_checksums_file_directory(options) .. pathSeparator .. options.accepted_checksums_file_name
end

local function accept_checksum(options, file_path)
	local checksum = plugin.get_checksum(file_path)
	local accepted_file = get_accepted_checksums_file(options)
	local directory = get_accepted_checksums_file_directory(options)
	if vim.fn.isdirectory(directory) == 0 then
		vim.fn.mkdir(directory, "p")
	end
	if not plugin.silent then
		vim.notify("Adding " .. file_path .. " with checksum " .. checksum .. " to " .. accepted_file)
	end
	os.execute("echo " .. checksum .. " " .. file_path .. " >> " .. accepted_file .. "\n")
	add_checksum_to_accepted_cache(checksum, file_path)
end

local function read_accepted_checksums(options)
	local accepted_file = get_accepted_checksums_file(options)
	if vim.fn.filereadable(accepted_file) ~= 1 then
		return
	end
	for line in io.lines(accepted_file) do
		local checksum
		for str in string.gmatch(line, "([^ ]+)") do
			checksum = str
			break
		end
		local file_path = line:sub(string.len(checksum) + 2, string.len(line))
		add_checksum_to_accepted_cache(checksum, file_path)
	end
end

local function is_file_accepted(file_path)
	local checksum = plugin.get_checksum(file_path)
	local ok = plugin.accepted_checksums[file_path] ~= nil and plugin.accepted_checksums[file_path] == checksum
	return ok
end

local function source_config_file(options, config_file_path, location_type)
	config_file_path = vim.fn.expand(config_file_path)
	if vim.fn.filereadable(config_file_path) == 1 then
		if location_type ~= plugin.location_types.ProjectsFolder then
			if not is_file_accepted(config_file_path) then
				if ask_user_to_accept_config(options, config_file_path) then
					accept_checksum(options, config_file_path)
				else
					return false
				end
			end
		end
		local loaded = false
		local status_ok = false
		if config_file_path:match("%.vim$") and options.load_vim_config then
			status_ok, _ = pcall(vim.cmd, "source " .. config_file_path)
			loaded = true
		elseif config_file_path:match("%.lua$") and options.load_lua_config then
			status_ok, _ = pcall(vim.cmd, "luafile " .. config_file_path)
			loaded = true
		end

		if loaded then
			if not status_ok then
				vim.notify('Error in configuration file "' .. config_file_path .. '"')
				return false
			else
				if plugin.after_config_loaded then
					plugin.after_config_loaded(config_file_path)
				end
			end
		end
		if not plugin.silent and loaded then
			vim.notify("Loaded configuration file " .. config_file_path)
		end
		return true
	end
	return false
end

local function get_file_path(options, location_type, file_name)
	return get_directory_from_location_type(options, location_type) .. pathSeparator .. file_name
end

local function get_default_new_config_file_path(options)
	local default_entry = options.potential_sources[1]
	local directory = get_directory_from_location_type(options, default_entry.location_type)

	if vim.fn.isdirectory(directory) == 0 then
		vim.fn.mkdir(directory, "p")
	end

	local config_file_path = get_file_path(options, default_entry.location_type, default_entry.file_name)

	return config_file_path
end

local function find_existing_config(options)
	for _, val in pairs(options.potential_sources) do
		local file_path = get_file_path(options, val.location_type, val.file_name)
		if vim.fn.filereadable(file_path) == 1 then
			return file_path
		end
	end
end

function plugin.load_project_config(custom_options)
	local options = custom_options or plugin.options
	read_accepted_checksums(options)
	local loaded = false
	for _, val in pairs(options.potential_sources) do
		if
			(val.location_type == plugin.location_types.CurrentWorkingDirectory and options.load_from_current_directory)
			or (val.location_type == plugin.location_types.ProjectsFolder and options.load_from_projects_directory)
		then
			if
				source_config_file(options, get_file_path(options, val.location_type, val.file_name), val.location_type)
			then
				loaded = true
			end
		end
	end

	if loaded then
		if plugin.after_all_config_loaded then
			plugin.after_all_config_loaded()
		end
	end
end

function plugin.edit_config(custom_options)
	local options = custom_options or plugin.options

	local config_file_path = find_existing_config(options)

	-- Edit new one if none exist already
	if config_file_path == nil then
		config_file_path = get_default_new_config_file_path(options)
	end

	vim.cmd("edit " .. config_file_path)
end

return plugin
