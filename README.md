# init_more.nvim
Neovim plugin that attempts to add support for loading more context aware neovim configuration files.

## Installation
Add this plugin with the `https://gitlab.com/devtoi/init_more.nvim` repo path to whatever manager you are using.

## Features
* [x] Load project specific configuration from a specific directory based on deduced project name
* [x] Load configuration file from the directory Neovim is launched from
* [x] Query user when loading configuration file from launch directory
	* [ ] Let user read file before accepting/denying it
	* [x] Add hash check for accepted configurations files to detect when they change (For basic added security)
* [x] Callbacks on load of configuration files
* [ ] Profiles
	* [ ] Lazy loaded

## Example scenario
You want to have some extra LSP settings for a specific project only. You then either create a `nvim_config.lua` file in that projects folder where you launch your neovim session from (eg. `~/projects/your_project_name/nvim_config.lua`), or you create the file `~/.config/nvim/projects/your_project_name/nvim_config.lua` (default path)

You then proceed to fill that file out with whatever configurations you want specific for that project and the add the plugin configuration to your normal neovim initialization file (eg. `~/.config/nvim/ini.lua`) as per the next section.

Upon launching neovim from your `~/projects/your_project_name` folder your project configuration will be loaded.

## Configuration
Loading configuration files that you haven't created can be dangerous, therefore the configuration defaults to having the feature for loading configurations from the current directory, turned off. To turn on add this to your normal neovim configuration file.

```lua
require("init_more").options.load_from_current_directory = true
```

The plugin requires you to call the function `require("init_more").load_project_config()`. This will load any potential relevant configuration files.

## Example configuration
```lua
local im = require "init_more"
-- Set the folder where project files are found and created, defaults to vim.fn.stdpath("config") .. "/projects/"
im.options.projects_directory = "~/.config/lvim/projects/"

-- Poses potential security risks, false by default
im.options.load_from_current_directory = true

 -- wheter to load configs from the im.options.projects_directory, true by default
im.options.load_from_projects_directory = true

-- Wheter to load .vim files, false by default
im.options.load_vim_config = true

-- Wheter to load lua files, true by default
im.options.load_lua_config = true

-- Callbacks for when a config file is loaded
im.after_config_loaded = function(file)
	print( "Loaded config " .. file)
end
-- Callback when the plugin is done loading config files
im.after_all_config_loaded = function()
	print( "Loaded configs done")
end

-- Load the actual project config files
im.load_project_config()
```
